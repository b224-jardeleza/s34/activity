const express = require("express");
const app = express();
const port = 8000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.get("/home", (request, response) => {
	response.send(`Welcome to the home page! :D`);
});

let users = [
	{	
		"username": "johndoe",
		"password": "johndoe1234"
	},
	{
		"username": "janesmith",
		"password": "janesmith1234"
	}
];


app.get("/users", (request, response) => {
	response.send(users);
});

app.delete("/delete-user", (request, response) => {

		let notify;

	for(let i=0; i < users.length; i++){
		if (request.body.username == users[i].username && request.body.password == users[i].password){
				users.splice(users[i], 1);
				notify = `User ${request.body.username} has been deleted.`
				break;
		} else if (request.body.username == users[i].username && request.body.password != users[i].password) {
				notify = `Unauthorized deletion of user ${request.body.username}.`
		} else {
			notify = `User ${request.body.username} not found.`

		}
	}
	response.send(notify);
	console.log(users);	
});



app.listen(port, () => console.log(`Server running at localhost: ${port}`));

